import React from 'react';
import logo from './logo.svg';
import anterior from './atras.svg';
import siguiente from './proximo.svg';
import './App.css';
import './script.js';

function App() {

  return (
    <form className="cajero">
        <div className="botoneria" >
            <button className="boton botonSiguiente"  ></button >
            <div className="boton botonSiguiente"></div>
            <div className="boton botonSiguiente"></div>
            <div className="boton botonSiguiente"></div>
        </div>
        <div className="centro">
            <div className="pantalla"></div>
            <div className="contenedor">
                <div className="recibo">
                    <div className="reciboTitulo">Recibo</div>
                    <div className="reciboIndicador"></div>
                    <div className="reciboApertura"></div>
                </div>
                <div className="insertarTarjeta">
                    <div className="tarjetaTitulo">Tarjeta</div>
                    <div className="tarjetaIndicador"></div>
                    <div className="tarjetaApertura"></div>
                </div>
                <div className="dispensadorEfectivo">      
                    <div className="dispensadorTitulo">Billetes</div>
                    <div className="dispensador">
                        <div className="dispensadorIndicador"></div>
                        <div className="dispensadorApertura"></div>
                    </div>
                </div>
            </div>
            <div className="teclado">
                <div className="botonTeclado"><label>7</label></div>
                <div className="botonTeclado"><label>8</label></div>
                <div className="botonTeclado"><label>9</label></div>                    
                <div className="botonTecladoG"><label>Borrar</label></div>
                <div className="botonTeclado"><label>4</label></div>
                <div className="botonTeclado"><label>5</label></div>
                <div className="botonTeclado"><label>6</label></div>
                <div className="botonTecladoG"><label>Cancelar</label></div>
                <div className="botonTeclado"><label>1</label></div>
                <div className="botonTeclado"><label>2</label></div>
                <div className="botonTeclado"><label>3</label></div>
                <div className="botonTecladoG"><label>Procesar</label></div>
                <div className="botonTeclado"><label></label></div>
                <div className="botonTeclado"><label>0</label></div>
                <div className="botonTeclado"><label></label></div>
                <div className="botonTecladoG"><label></label></div>
            </div>
        </div>
        <div className="botoneria">
            <div className="boton botonAnterior"></div>
            <div className="boton botonAnterior"></div>
            <div className="boton botonAnterior"></div>
            <div className="boton botonAnterior"></div>
        </div>
    </form>
  );
}

export default App;
